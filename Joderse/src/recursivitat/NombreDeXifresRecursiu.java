package recursivitat;

import java.util.Scanner;

/*
 * https://joder.ga/problem/nxifresrec
 */

public class NombreDeXifresRecursiu {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			int num = sc.nextInt();
			
			int xifres = contar(num);
			
			System.out.println(xifres);
		}
	}

	private static int contar(int num) {
		
		if(num < 10) {
			return 1;
		}else {
			return 1 + contar(num/10);
		}
	}

}
