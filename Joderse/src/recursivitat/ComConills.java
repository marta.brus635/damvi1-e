package recursivitat;

import java.util.Scanner;

/*
 * https://joder.ga/problem/comconills
 */

public class ComConills {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			int num = sc.nextInt();
			
			int resultat = fibonacci(num);
			
			System.out.println(resultat);
		}
	}

	private static int fibonacci(int num) {
		
		if(num == 1) {
			return 1;
		}else if(num == 2) {
			return 1;
		}else {
			return fibonacci(num-2) + fibonacci(num-1);
		}
	}

}
