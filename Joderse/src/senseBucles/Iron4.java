package senseBucles;

import java.util.Scanner;

/*
 * https://joder.ga/problem/iron4
 */

public class Iron4 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		String letra = sc.nextLine();

		switch (letra) {
		case "Q":
			System.out.println("Skill 1");
			break;

		case "W":
			System.out.println("Skill 2");
			break;

		case "E":
			System.out.println("Skill 3");
			break;

		case "R":
			System.out.println("Ultimate");
			break;

		case "B":
			System.out.println("Recall");
			break;

		case "D":
		case "F":
			System.out.println("Bronzes never use summoners");
			break;

		default:
			System.out.println("Error");
		}
	}

}
