package senseBucles;

import java.util.Scanner;

/*
 * https://joder.ga/problem/pokerdaus
 */

public class PokerDeDausATemeria {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int a = sc.nextInt();
		int b = sc.nextInt();
		int c = sc.nextInt();
		int d = sc.nextInt();

		if (a == b && a == c && a == d) {
			System.out.println("POKER");

		} else if ((a == b && a == c) 
				|| (a == b && a == d) 
				|| (a == c && a == d) 
				|| (b == c && b == d)) {
			System.out.println("TRIO");

		} else if (a == b || a == c || a == d || b == c || b == d || c == d) {
			System.out.println("PARELLA");

		} else {
			System.out.println("RES");
		}
	}

}
