package senseBucles;

import java.util.Scanner;

/*
 * https://joder.ga/problem/pedrapaper
 */

public class PedraPaperTisores {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int posicion1 = sc.nextInt();
		int posicion2 = sc.nextInt();
		
		if (posicion1 == 1 && posicion2 == 3 
				|| posicion1 == 2 && posicion2 == 1 
				|| posicion1 == 3 && posicion2 == 2) {
			
			System.out.println("Jugador1");
			
		}else if(posicion2 == 1 && posicion1 == 3 
				|| posicion2 == 2 && posicion1 == 1 
				|| posicion2 == 3 && posicion1 == 2){
			
			System.out.println("Jugador2");
			
		}else if (posicion1 == 0 || posicion2 == 0 ) {
			System.out.println("ERROR");
			
		}else if (posicion1 == posicion2) {
			System.out.println("EMPAT");
		}
	}

}
