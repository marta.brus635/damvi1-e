package senseBucles;

import java.util.Scanner;

/*
 * https://joder.ga/problem/diferenciagranpetit
 */

public class DiferenciaGranPetit {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int a = sc.nextInt();
		int b = sc.nextInt();
		int aux = 0;
		
		if (a < b) {
			aux = b - a;
			
		} else {
			aux = a - b;
			
		}
		
		System.out.println(aux);
		
	}

}
