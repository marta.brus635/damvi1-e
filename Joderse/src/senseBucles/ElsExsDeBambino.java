package senseBucles;

import java.util.Scanner;

/*
 * https://joder.ga/problem/exsbambino
 */

public class ElsExsDeBambino {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int A = sc.nextInt();
		int B = sc.nextInt();
		int C = sc.nextInt();
		int N = sc.nextInt();

		if (N == A || N == B || N == C) {
			System.out.println("NO");
			
		} else {
			System.out.println("SI");
			
		}
	}

}
