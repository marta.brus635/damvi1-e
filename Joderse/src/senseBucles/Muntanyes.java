package senseBucles;

import java.util.Scanner;

/*
 * https://joder.ga/problem/muntanyes
 */

public class Muntanyes {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int a = sc.nextInt();
		int b = sc.nextInt();
		int c = sc.nextInt();
		int d = sc.nextInt();
		int e = sc.nextInt();

		if (b > a && c < b && d > c && e < d) {
			System.out.println("SI");
			
		}else if ( b < a && c > b && d < c && e > d){
			System.out.println("SI");
			
		}else {
			System.out.println("NO");
		}
	}

}
