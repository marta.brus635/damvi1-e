package senseBucles;

import java.util.Scanner;

/*
 * https://joder.ga/problem/quadrat
 */

public class Quadrat {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int base = sc.nextInt();
		
		int r = base * base;
		
		System.out.println(+r);
	}

}
