package senseBucles;

import java.util.Scanner;

/*
 * https://joder.ga/problem/buscarparelles
 */

public class CercarParelles {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int a = sc.nextInt();
		int b = sc.nextInt();
		int c = sc.nextInt();
		
		if ( a == b || a == c || c == b) {
			System.out.println("SI");
			
		}else {
			System.out.println("NO");
		}
	}

}
