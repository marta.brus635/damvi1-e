package senseBucles;

import java.util.Scanner;

/*
 * https://joder.ga/problem/eleccions20202
 */

public class Eleccions20202 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int Jiden = sc.nextInt();
		int Drump = sc.nextInt();

		if (Jiden > Drump) {
			System.out.println("Jiden");

		} else if (Drump > Jiden) {
			System.out.println("Drump");

		} else {
			System.out.println("No");
		}
	}

}
