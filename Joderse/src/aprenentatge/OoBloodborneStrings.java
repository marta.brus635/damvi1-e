package aprenentatge;

import java.util.Scanner;

public class OoBloodborneStrings {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int casos = sc.nextInt();
		sc.nextLine();
		
		for (int i = 0; i < casos; i++) {
			String palabrita = sc.nextLine();
			int ultimaPosicion = palabrita.length()-1;
			boolean flag = false;

			for (int j = 1; j <= ultimaPosicion; j++) {
				char primera = palabrita.charAt(j);
				char segunda = palabrita.charAt(j-1);
				if (primera == segunda) {
					flag = true;
				}
			}

			if (flag) {
				System.out.println("SI");
			} else {
				System.out.println("NO");
			}

		}

	}

}
