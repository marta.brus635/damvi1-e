package aprenentatge;

import java.util.Scanner;

public class SumaDeMatrius {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int num = sc.nextInt();
		
		int[][] matriu1 = new int[num][num];
		int[][] matriu2 = new int[num][num];
		
		
		for (int f = 0; f < num; f++) {
			for (int c = 0; c < num; c++) {
				matriu1[f][c] = sc.nextInt();
			}
		}
		
		for (int f = 0; f < num; f++) {
			for (int c = 0; c < num; c++) {
				matriu1[f][c] += sc.nextInt();
				System.out.print(matriu1[f][c]+" ");
			}
			System.out.println();
		}
		
	}

}
