package aprenentatge;

import java.util.Scanner;

public class Murcielago {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		String palabrita = sc.nextLine();
		int longitudString = palabrita.length();
		boolean a = false, e = false, i = false, o = false, u = false;
		
		for (int j = 0; j < longitudString; j++) {
			char c = palabrita.charAt(j);
			
			if (c == 'a') {
				a = true;
				
			}else if (c == 'e') {
				e = true;
				
			}else if (c == 'i') {
				i = true;
				
			}else if (c == 'o') {
				o = true;
				
			}else if (c == 'u') {
				u = true;
				
			}
		}
		
		if (a && e && i && o && u) {
			System.out.println("TOTES");
			
		}else {
			System.out.println("FALTEN");
		}
	}

}
