package aprenentatge;

import java.util.Scanner;

public class DobleBucle {

	public static void main(String[] args) {

		Scanner reader = new Scanner(System.in);

		int num = reader.nextInt();

		for (int i = 0; i <= num; i++) {
			
			for (int j = 0; j < i; j++) {
				System.out.print(i);
			}
		}
	}

}
