package aprenentatge;

import java.util.ArrayList;
import java.util.Scanner;

public class ComplintRątios {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int casos = sc.nextInt();

		for (int i = 0; i < casos; i++) {
			int palabritas = sc.nextInt();
			sc.nextLine();
			ArrayList<String> lista = new ArrayList<>();

			for (int j = 0; j < palabritas; j++) {
				lista.add(sc.next());
			}

			int pos = sc.nextInt();
			lista.remove(pos);

			for (int k = 0; k < lista.size(); k++) {
				System.out.print(lista.get(k)+" ");
			}
			System.out.println();
		}
	}

}
