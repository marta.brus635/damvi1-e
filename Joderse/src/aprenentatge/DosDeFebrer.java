package aprenentatge;

import java.util.ArrayList;
import java.util.Scanner;

public class DosDeFebrer {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		String numerito = sc.nextLine();
		int ultimaPosicion = numerito.length()-1;
		boolean flag = true;
		
		for (int i = 0; i < ultimaPosicion; i++) {
			char primera = numerito.charAt(i);
			char ultima = numerito.charAt(ultimaPosicion - i);
			
			if(primera != ultima) {
				flag = false;
			}
		}
		
		if(flag) {
			System.out.println("SI");
		}else {
			System.out.println("NO");
		}
	}

}
