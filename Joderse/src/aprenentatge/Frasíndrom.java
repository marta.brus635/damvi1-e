package aprenentatge;

import java.util.Scanner;

public class Frasíndrom {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		String frase = sc.nextLine();

		while (!frase.equals(".")) {
			String[] array1 = frase.split(" ");
			int ultima = array1.length - 1;
			boolean flag = true;

			for (int i = 0; i < ultima; i++) {
				if (!array1[i].equals(array1[ultima - i])) {
					flag = false;
				}
			}

			if (flag) {
				System.out.println("SI");
			} else {
				System.out.println("NO");
			}
			
			frase = sc.nextLine();
		}

	}

}
