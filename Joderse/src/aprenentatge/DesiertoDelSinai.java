package aprenentatge;

import java.util.LinkedHashMap;
import java.util.Scanner;

public class DesiertoDelSinai {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++)
		{
			LinkedHashMap<String, Integer> diccionari = new LinkedHashMap<String, Integer>();
			int mapas = sc.nextInt();
			sc.nextLine();
			
			for (int j = 0; j < mapas; j++) {
				String mapita = sc.nextLine();
				
				if (diccionari.containsKey(mapita)) {
					int valor = diccionari.get(mapita);
					valor++;
					diccionari.put(mapita, valor);
					
				}else {
					diccionari.put(mapita, 1);
				}
			}
			
			int votacio  = 0;
			String mapaGanador = null;
			
			for (String nom : diccionari.keySet()) {
				if(diccionari.get(nom)>votacio) {
					votacio = diccionari.get(nom);
					mapaGanador = nom;
				}
			}
			
			System.out.println(mapaGanador);
		}
	}

}
