package aprenentatge;

import java.util.Arrays;
import java.util.Scanner;

public class FrasePalindrom {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		sc.nextLine();
		
		for (int i = 0; i < casos; i++) {
			String linea = sc.nextLine();
			String lineaLimpia = linea.replace(" ", "");
			lineaLimpia = lineaLimpia.toLowerCase();
			char[] caracteres = lineaLimpia.toCharArray();
			
			int longitud = caracteres.length-1;
			int mitad = longitud / 2;
			int paresinones = longitud % 2;
			
			if (paresinones == 1) {
				mitad+=1;
			}
			
			char[] primera = new char[mitad];
			
			for (int j = 0; j < primera.length; j++) {
				primera[j] = caracteres[j];
			}
			
			char[] segunda = new char[mitad];
			int contador = 0;
			
			for (int k = longitud; k > mitad; k--) {
				segunda[contador] = caracteres[k];
				contador++;
			}
			
			
			if (Arrays.equals(primera, segunda)) {
				System.out.println("SI");
				
			}else {
				System.out.println("NO");
			}
		}

	}

}
