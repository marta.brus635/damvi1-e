package matrius;

import java.util.Scanner;

/*
 * https://joder.ga/problem/intromatrius1
 */

public class EscriuEnUnaMatriu1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int filas = sc.nextInt();
		int columnas = sc.nextInt();
		int[][] matriu = new int[filas][columnas];
		
		for (int f = 0; f < filas; f++) {
			for (int c = 0; c < columnas; c++) {
				matriu[f][c] = sc.nextInt();
			}
		}
		
		int filaB = sc.nextInt();
		int columnaB = sc.nextInt();
		
		for (int f = 0; f < filas; f++) {
			for (int c = 0; c < columnas; c++) {
				System.out.print(matriu[f][c]+" ");
			}
			System.out.println();
		}
		
		System.out.println(matriu[filaB][columnaB]);
	}

}
