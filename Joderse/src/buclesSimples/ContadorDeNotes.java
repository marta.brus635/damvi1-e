package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/contadornotes
 */

public class ContadorDeNotes {

	public static void main(String[] args) {

		Scanner michi = new Scanner(System.in);
		
		int nota = michi.nextInt();
		int e = 0;
		int n = 0;
		int b = 0;
		int s = 0;
		int i = 0;
		int md = 0;
		float mitjana = 0;
		int total = 0;
		float resultat = 0;
		
		
		while (nota != -1) {
			
			if (nota >= 9 && nota <= 10){
				e++;
				total++;
				mitjana = mitjana + nota;
				
			} else if (nota >= 7 && nota < 9) {
				n++;
				total++;
				mitjana = mitjana + nota;
				
			}else if (nota >= 6 && nota < 7) {
				b++;
				total++;
				mitjana = mitjana + nota;
				
			}else if (nota >= 5 && nota < 6) {
				s++;
				total++;
				mitjana = mitjana + nota;
				
			}else if (nota > 3 && nota < 5) {
				i++;
				total++;
				mitjana = mitjana + nota;
				
			}else if (nota >= 0 && nota <= 3){
				md++;
				total++;
				mitjana = mitjana + nota;
				
			}
			
			nota = michi.nextInt();
		}
		
		resultat = mitjana / total;
		System.out.println("NOTES: "+total+" MITJANA: "+resultat+" E: "+e+" N: "+n+" B: "+b+" S: "+s+" I: "+i+" MD: "+md);
	}

}
