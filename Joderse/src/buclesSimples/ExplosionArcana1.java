package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/explosionarcana1
 */

public class ExplosionArcana1 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int ini = sc.nextInt();
		int a = ini;
		int q = sc.nextInt();
		int total = 0;
		
		
		for (int i = 0; i < q; i++) {
			total = total + a;
			a = a + ini;
			
		}
		
		System.out.println(total);
	}

}