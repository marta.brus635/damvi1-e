package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/ambrosio
 */

public class Ambrosio {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		int bombons = 0;
		
		for (int i = 0; i < casos; i++) {
			int pis = sc.nextInt();
			int acumulador = 0;
			
			for (int j = 1; j <= pis; j++) {
				bombons = j*j;
				acumulador += bombons;
			}
			
			System.out.println(acumulador);
		}
	}

}
