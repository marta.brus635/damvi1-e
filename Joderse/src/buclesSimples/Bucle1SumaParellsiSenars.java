package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/senarsparells
 */

public class Bucle1SumaParellsiSenars {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			int numerito = sc.nextInt()+1;
			int parell = 0;
			int senar = 0;
			
			for (int j = 0; j < numerito; j++) {
				int aux = j % 2;
				
				if(aux == 0) {
					parell+=j;
				}
				
				if(aux == 1) {
					senar+=j;
				}
					
			}
			
			System.out.println("PARELLS: "+parell+" SENARS: "+senar);
		}

	}

}
