package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/tracacovid
 */

public class Trašacovid {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int casos = sc.nextInt();
		int dias = 0;
		int persona = 0;
		boolean covid = false;

		for (int i = 0; i < casos; i++) {
			dias = sc.nextInt();

			for (int j = 0; j < dias; j++) {
				persona = sc.nextInt();

				if (persona >= 300) {
					covid = true;
				}
			}

			if (covid == true) {
				System.out.println("ALARMA");

			} else {
				System.out.println("OK");
			}
			covid = false;
		}

	}

}