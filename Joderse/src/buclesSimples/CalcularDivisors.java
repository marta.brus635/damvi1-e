package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/divisors
 */

public class CalcularDivisors {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			int num = sc.nextInt();
			int aux = 0;
			
			for (int j = 0; j <= num; j++) {
				if (j != 0) {
					aux = num % j;
				}
				
				if (aux == 0 && j != 0) {
					System.out.print(j+" ");
				}
			}
			System.out.println();
		}
	}

}
