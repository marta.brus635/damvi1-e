package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/contarvocals
 */

public class ContarVocals {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		sc.nextLine();
		
		for (int j = 0; j < casos; j++) {
			String frase = sc.nextLine();
			frase = frase.toLowerCase();
			int a = 0, e = 0, i = 0, o = 0, u = 0;
			
			for (int k = 0; k < frase.length(); k++) {
				char letrita = frase.charAt(k);
				
				if(letrita == 'a') {
					a++;	
				}
				
				if(letrita == 'e') {
					e++;	
				}
				
				if(letrita == 'i') {
					i++;
				}
				
				if(letrita == 'o') {
					o++;	
				}
				
				if(letrita == 'u') {
					u++;
				}
			}
			
			System.out.println("A: "+a+" E: "+e+" I: "+i+" O: "+o+" U: "+u);
		}
	}

}
