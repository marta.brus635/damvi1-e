package buclesSimples;

import java.util.Scanner;

/*
 * https://joder.ga/problem/presidentpatata
 */

public class PresidentPatata {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			int vegades = sc.nextInt();
			
			for (int j = 0; j < vegades; j++) {
				System.out.println("No ofendre al president patata");
				
			}
		}
	}

}
