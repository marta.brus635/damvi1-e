package diccionaris;

import java.util.LinkedHashMap;
import java.util.Scanner;

/*
 * https://joder.ga/problem/introdicc
 */

public class EscriuEnUnDiccionari1 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		LinkedHashMap<String, String> diccionari = new LinkedHashMap<String, String>();
		
		int cosas = sc.nextInt();
		sc.nextLine();
		
		for (int i = 0; i < cosas; i++) {
			diccionari.put(sc.nextLine(), sc.nextLine());
		}
		
		System.out.println(diccionari);
		String cumple = diccionari.getOrDefault(sc.nextLine(), " ");
		System.out.println(cumple);
		
	}

}
