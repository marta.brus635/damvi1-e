package array_llistes;

import java.util.Scanner;

/*
 * https://joder.ga/problem/introarrays1
 */

public class EscriuUnArray1 {

	public static void main(String[] args) {	
		
		Scanner michi = new Scanner(System.in);
		
		int k = michi.nextInt();
		
		int[] a = new int[k];
		
		for (int j = 0; j < a.length; j++) {
			a[j] = michi.nextInt();
		}

		int n = michi.nextInt();
		
		for (int i = 0; i < a.length; i++) {
			System.out.print(a[i]+" ");
		}
		System.out.println();
		System.out.println(a[n]);
		
	}

}
