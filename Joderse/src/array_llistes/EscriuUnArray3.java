package array_llistes;

import java.util.ArrayList;
import java.util.Scanner;

/*
 * https://joder.ga/problem/introarrays3
 */

public class EscriuUnArray3 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		ArrayList<Integer> llista = new ArrayList<>();
		
		int num = sc.nextInt();
		
		while (num != -1) {
			llista.add(num);
			num = sc.nextInt();
		}
		
		int posicio = sc.nextInt();
		System.out.println(llista);
		System.out.println(llista.get(posicio));
	}

}
