package array_llistes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/*
 * https://joder.ga/problem/totsaladreta
 */

public class TotsALaDreta {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int numeritos = sc.nextInt();
		
		ArrayList<Integer> llista = new ArrayList<>();
		
		for (int i = 0; i < numeritos; i++) {
			llista.add(sc.nextInt());
		}
		
		Collections.rotate(llista, 1);
		
		for (int i = 0; i < llista.size(); i++) {
			System.out.print(llista.get(i)+" ");
		}
		
		System.out.println();
	}

}
