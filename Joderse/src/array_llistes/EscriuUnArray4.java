package array_llistes;

import java.util.Scanner;

/*
 * https://joder.ga/problem/introarrays4
 */

public class EscriuUnArray4 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int k = sc.nextInt();
		
		int[] a = new int[k];
		
		
		for (int i = 0; i < a.length; i++) {
			a[i] = sc.nextInt();
		}
		
		int suma = sc.nextInt();
		
		for (int j = 0; j < a.length; j++) {
			System.out.print(a[j]+suma+" ");
		}
	}

}