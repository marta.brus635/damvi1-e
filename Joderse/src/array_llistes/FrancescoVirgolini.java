package array_llistes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

/*
 * https://joder.ga/problem/francescovirgolini
 */

public class FrancescoVirgolini {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		
		for (int i = 0; i < casos; i++) {
			int longitud = sc.nextInt();
			sc.nextLine();
			ArrayList<String> llistaString = new ArrayList<>();
			
			for (int j = 0; j < longitud; j++) {
				llistaString.add(sc.nextLine());
			}
			
			int posicion = llistaString.indexOf("Francesco Virgolini");
			Collections.swap(llistaString, posicion, posicion-1);
			System.out.println(llistaString);
			
		}

	}

}