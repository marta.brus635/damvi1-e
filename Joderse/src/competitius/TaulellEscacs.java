package competitius;

import java.util.Scanner;

/*
 * https://joder.ga/problem/taulellescacs
 */

public class TaulellEscacs {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		int files = sc.nextInt();
		int columnes = sc.nextInt();
		boolean flag = true;
		
		for (int f = 0; f < files; f++) {
			for (int c = 0; c < columnes; c++) {
				if (flag) {
					System.out.print(". ");
					flag = !flag;
					
				} else {
					System.out.print("# ");
					flag = !flag;
					
				}
			}
			
			flag = !flag;
			System.out.println();
		}
	}

}
