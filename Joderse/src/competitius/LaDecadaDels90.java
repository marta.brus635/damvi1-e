package competitius;

import java.util.Scanner;

/*
 * https://joder.ga/problem/decada90
 */

public class LaDecadaDels90 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		String a = sc.nextLine();
		
		int longitud = a.length();
		char num1 = a.charAt(longitud-2);
		
		if (num1 == '9') {
			System.out.println("SI");
			
		}else {
			System.out.println("NO");
		}
	}

}
