package competitius;

import java.util.ArrayList;
import java.util.Scanner;

/*
 * https://joder.ga/problem/pozoluna
 */

public class DelPozoALaLuna {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int casos = sc.nextInt();
		boolean flag = false;
		
		for (int i = 0; i < casos; i++) {
			int numPalabritas = sc.nextInt();
			sc.nextLine();
			ArrayList<String> llista = new ArrayList<>();
			
			for (int j = 0; j < numPalabritas; j++) {
				llista.add(sc.nextLine());
			}
			
			for (int j = 1; j < llista.size()-1; j++) {
				String palabrita1 = llista.get(j);
				String palabrita2 = llista.get(j-1);
				int longitudPalabrita1 = palabrita1.length();
				int longitudPalabrita2 = palabrita2.length();
				int letras = 0;
				
				if(longitudPalabrita1 == longitudPalabrita2) {
					for (int k = 0; k < palabrita1.length(); k++) {
						char letrita1 = palabrita1.charAt(k);
						char letrita2 = palabrita2.charAt(k);
						
						if (letrita1 != letrita2) {
							letras++;
						}
						
					}
					
					if (letras != 1) {
						flag = true;
					}
					
				}else {
					flag = true;
				}
			}
			
			if(flag) {
				System.out.println("INCORRECTE");
			}else {
				System.out.println("CORRECTE");
			}
			
			flag = false;
		}
	}

}
