package niNoKuni;

public abstract class Enemic extends Personatge{

	protected int odi;
	
	public Enemic(int vida, int atac, int odi) {
		super(vida, atac);
		this.odi = odi;
	}
	
}
