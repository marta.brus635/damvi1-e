package niNoKuni;

public class Marcs extends Aliat{

	String uni;
	
	public Marcs(String nom, int vida, int atac, String uni) {
		super(nom, vida, atac);
		this.uni = uni;
	}

	@Override
	public void atacar(Personatge a) {
		
		if(!this.mort && !a.mort) {
			if(a instanceof Enemic) {
				a.vidaActual -= this.atac;
				
				if(a.vidaActual <= 0) {
					a.morirse();
				}
				
				System.out.println("por maricona");
				
			} else {
				System.out.println("Estas intentant atacar a un aliat!");
			}
			
		}else {
			System.out.println("Hi ha personatges morts");
		}
	}

}
