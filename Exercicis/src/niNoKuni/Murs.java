package niNoKuni;

public class Murs extends Enemic{

	public Murs(int vida, int atac, int odi) {
		super(vida, atac, odi);
		
	}

	@Override
	public void atacar(Personatge a) {

		if(!this.mort && !a.mort) {
			if(a instanceof Aliat) {
				a.vidaActual -= this.atac;
				
				if(a.vidaActual <= 0) {
					a.morirse();
				}
				
				System.out.println("pum");
				
			} else {
				System.out.println("Estas intentant atacar a un Enemic!");
			}
			
		}else {
			System.out.println("Hi ha personatges morts");
		}
		
	}

	
}
