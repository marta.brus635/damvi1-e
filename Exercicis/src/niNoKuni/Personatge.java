package niNoKuni;

public abstract class Personatge {

	protected int vidaMax;
	protected int vidaActual;
	protected int atac;
	protected boolean mort;
	
	public Personatge(int vida, int atac) {
		this.vidaMax = vida;
		this.vidaActual = vida;
		this.atac = atac;
		this.mort = false;
	}

	public void morirse() {
		this.mort = true;
	}

	public String visualitzar() {
		return "Personatge [vidaActual=" + vidaActual + ", atac=" + atac + ", getClass()=" + getClass() + "]";
	}
	
	public abstract void atacar(Personatge a) throws Exception;

}
