package niNoKuni;

public class TapesClaveguera extends Enemic implements ArmaDelMal{

	public TapesClaveguera(int vida, int atac, int odi) {
		super(vida, atac, odi);
		
	}

	@Override
	public void atacar(Personatge a) {
		
		if(!this.mort && !a.mort) {
			if(a instanceof Aliat) {
				a.vidaActual -= this.atac;
				
				if(a.vidaActual <= 0) {
					a.morirse();
				}
				
				System.out.println("AAAAAAAAAAAA");
				
			} else {
				System.out.println("Estas intentant atacar a un enemic!");
			}
			
		}else {
			System.out.println("Hi ha personatges morts");
		}
		
	}

	@Override
	public void riureMaligne() {
		
	}

	
}
