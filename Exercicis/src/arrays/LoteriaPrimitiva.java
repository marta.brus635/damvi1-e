package arrays;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.Scanner;

public class LoteriaPrimitiva {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		Random r = new Random();
		
		boolean exit = false;
		int opcio = 0;
		ArrayList<Integer> combinacio = new ArrayList<Integer>();
		ArrayList<Integer> sorteig = new ArrayList<Integer>();
		
		System.out.println("--------------------------------------------------------------------");
		System.out.println("-                      LOTERIA PRIMITIVA                           -");
		System.out.println("--------------------------------------------------------------------");
		System.out.println();
		
		do {
			
			System.out.println("--------------------------------------------------------------------");
			System.out.println("-                           - MENU -                               -");
			System.out.println();
			System.out.println("- Pulsa 1 para introducir una combinaci�n de 6 n�meros enteros");
			System.out.println("- Pulsa 2 para generar un sorteo de 6 n�meros enteros aleatorios");
			System.out.println("- Pulsa 3 para comprovar si has ganado un premio!!!!");
			System.out.println("- Pulsa 4 para ver la combinaci�n de n�meros que has introducido");
			System.out.println("- Pulsa 5 para ver el sorteo de n�meros que se ha generado");
			System.out.println("- Pulsa 6 para borrar la combinaci�n y el sorteo de n�meros");
			System.out.println("- Pulsa 0 para salir :(");
			System.out.println("--------------------------------------------------------------------");
			
			opcio = sc.nextInt();
			
			switch(opcio) {
			case 1:
				System.out.println();
				System.out.println("--------------------------------------------------------------------");
				System.out.println("Introduce 6 n�meros ENTEROS");
				System.out.println("--------------------------------------------------------------------");
				
				while(combinacio.size() < 6) {
					int num = sc.nextInt();
					
					if(!combinacio.contains(num)) {
						combinacio.add(num);
						
					}else {
						System.out.println("No se pueden insertar n�meros repetidos");
					}
				}
				System.out.println("--------------------------------------------------------------------");
				System.out.println("Introducci�n de n�meros completada :)");

				break;
				
			case 2:
				while(sorteig.size() < 6) {
					int num = r.nextInt((48)+1);
					
					if(!sorteig.contains(num)) {
						sorteig.add(num);
					}
				}
				
				System.out.println();
				System.out.println("--------------------------------------------------------------------");
				System.out.println("Se han generado 6 n�meros aleatorios correctamente :)");
				break;
				
			case 3:
				int contador = 0;
				
				for (int i = 0; i < 6; i++) {
					int numero = combinacio.get(i);
					
					if(sorteig.contains(numero)) {
						contador++;
					}
				}
				
				System.out.println("Has acertado "+contador+" n�meros");
				break;
				
			case 4:
				System.out.println();
				System.out.println("--------------------------------------------------------------------");
				System.out.println("Estos son los n�meros que has insertado: ");
				System.out.println(combinacio);
				break;
				
			case 5:
				System.out.println();
				System.out.println("--------------------------------------------------------------------");
				System.out.println("Estos son los n�meros generados: ");
				System.out.println(sorteig);
				break;
				
			case 6:
				combinacio.clear();
				sorteig.clear();
				System.out.println();
				System.out.println("--------------------------------------------------------------------");
				System.out.println("Las 2 listas se han borrado correctamente :)");
				break;
				
			case 0:
				exit = true;
				break;
				
			default:
				System.out.println("Eres tonto!! elige una opcion correcta >:(");
			}
			
		}while (!exit);
		
		System.out.println("Adeu :)");
	}

}
