package funcions;

import java.util.Scanner;

public class CalculadoraFuncions {
	
	static Scanner sc = new Scanner(System.in);;
	
	public static void main(String[] args) {
		
		System.out.println("CALCULADORA");
		System.out.println("Selecciona una opci�: ");
		boolean exit = false;
		int aux = 0;
		int num1 = 0;
		int num2 = 0;
		
		do {
			
			presentarMenu();
			int opcio = sc.nextInt();
			
			switch(opcio) {
			case 1:
				if(aux == 0) {
					System.out.println("Insereix el primer n�mero");
					num1 = obtenirNumero();
					System.out.println("Insereix el segon n�mero");
					num2 = obtenirNumero();
					
				} else {
					System.out.println("Has de reiniciar el programa per inserir numeros diferents");
					
				}
				break;
				
			case 2:
				aux = sumar(num1, num2);
				break;
				
			case 3:
				aux = restar(num1, num2);
				break;
				
			case 4:
				aux = multiplicar(num1, num2);
				break;
				
			case 5:
				aux = divisio(num1, num2);
				break;
				
			case 6:
				System.out.println(aux);
				break;
				
			case 7:
				exit = true;
				break;
				
			default:
				System.err.println("Si us plau selecciona una opci� llistada");
				break;
			}
			
		}while(!exit);
		
		System.out.println("A10");
	}
	
	public static void presentarMenu() {
		
		System.out.println("1- Inserir 2 n�meros");
		System.out.println("2- Sumar");
		System.out.println("3- Restar");
		System.out.println("4- Multiplicar");
		System.out.println("5- Divisi�");
		System.out.println("6- Visualitzar el resultat");
		System.out.println("7- Sortir del programa");
	}
	
	public static int obtenirNumero() {
		
		sc = new Scanner(System.in);
		
		return(sc.nextInt());
		
	}
	
	public static int sumar(int num1, int num2) {
		
		num1 += num2;
		return(num1);
		
	}
	
	public static int restar(int num1, int num2) {
		
		num1 -= num2;
		return(num1);
		
	}
	
	public static int multiplicar(int num1, int num2) {
		
		num1 *= num2;
		return(num1);
		
	}
	
	public static int divisio(int num1, int num2) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Selecciona 1 si vols dividir");
		System.out.println("Selecciona 2 si vols el residu de la divisi�");
		
		int algo = sc.nextInt();
		
		switch (algo) {
		case 1:
			num1 /= num2;
			break;
			
		case 2:
			num1 %= num2;
			break;
		}
		
		return(num1);
	}
}

