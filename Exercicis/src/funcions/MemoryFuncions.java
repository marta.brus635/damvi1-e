package funcions;

import java.util.Random;
import java.util.Scanner;

public class MemoryFuncions {

	static Scanner sc;
	static Random r;

	public static void main(String[] args) {
		
		sc = new Scanner(System.in);
		r = new Random();
		
		System.out.println("MEMORY"); 
		String[][] tauler = new String[4][4];
		String[][] secret = new String[4][4];
		boolean exit = false;

		do {
			
			presentarMenu();
			int opcio = sc.nextInt();

			switch (opcio) {
			case 1: 
				tauler = iniciarTauler(tauler);
				secret = iniciarTauler(secret);
				System.out.println("Taulell inicialitzat!! :)");
				System.out.println();
				break;

			case 2: 
				mostrarTauler(tauler);
				System.out.println();
				break;

			case 3: 
				secret = obtenirPeces(secret);
				System.out.println();
				break;

			case 4: 
				secret = desordenar(secret);
				System.out.println();
				break;
			
			case 5: 
				triarCasella(tauler, secret);
				break;
				
			case 6: 
				ferJugada(tauler, secret);
				break;
				
			case 7: 
				tauler = iniciarTauler(tauler);
				secret = iniciarTauler(secret);
				secret = obtenirPeces(secret);
				secret = desordenar(secret);
				partidaSolitaria(tauler, secret);
				break;
				
			case 8: 
				tauler = iniciarTauler(tauler);
				secret = iniciarTauler(secret);
				secret = obtenirPeces(secret);
				secret = desordenar(secret);
				partidaMultijuadors(tauler, secret);
				break;
				
			case 0:
				exit = true;
				break;

			default: 
				System.out.println("Si us plau selecciona una opci� llistada.");

			}

		} while (!exit);

	}

	/**
	 * Presenta el men� para escoger una de todas las opciones listadas.
	 */
	
	public static void presentarMenu() {

		System.out.println("Selecciona una opci�: ");
		System.out.println("1- Inicialitzar tauler");
		System.out.println("2- Mostrar tauler");
		System.out.println("3- Posar peces");
		System.out.println("4- Remenar peces");
		System.out.println("5- Buscar parella");
		System.out.println("6- Fer jugada");
		System.out.println("7- Partida solitaria");
		System.out.println("8- Partida 2 jugadors");
		System.out.println("0- Sortir");

	}

	/**
	 * Inicia la matriz con las casillas en "?" (tapadas)
	 * @param tauler la matriz que le paso
	 * @return tauler la matriz que devuelvo
	 */
	
	public static String[][] iniciarTauler(String[][] tauler) { 

		for (int f = 0; f < 4; f++) {
			for (int c = 0; c < 4; c++) {
				tauler[f][c] = "?";
			}
		}

		return tauler;

	}

	/**
	 * Muestra el tablero por consola.
	 * @param tauler la matriz que le paso
	 */
	
	public static void mostrarTauler(String[][] tauler) { 

		for (int f = 0; f < 4; f++) {
			for (int c = 0; c < 4; c++) {
				System.out.print(tauler[f][c] + " ");
			}
			System.out.println();
		}

	}

	/**
	 * Inicia la matriz 4x4 con letras parejas.
	 * @param matriu la matriz que le paso
	 * @return tauler la matriz que devuelvo
	 */
	
	public static String[][] obtenirPeces(String[][] matriu) { 

		int contador = 0;
		
		for (int f = 0; f < 4; f++) {
			for (int c = 0; c < 4; c++) {
				if (contador < 2) {
					matriu[f][c] = "A";
					contador++;
				} else if (contador < 4) {
					matriu[f][c] = "B";
					contador++;
				} else if (contador < 6) {
					matriu[f][c] = "C";
					contador++;
				} else if (contador < 8) {
					matriu[f][c] = "D";
					contador++;
				} else if (contador < 10) {
					matriu[f][c] = "E";
					contador++;
				} else if (contador < 12) {
					matriu[f][c] = "F";
					contador++;
				} else if (contador < 14) {
					matriu[f][c] = "G";
					contador++;
				} else if (contador < 16) {
					matriu[f][c] = "H";
					contador++;
				}
			}
		}

		return matriu;
	}

	/**
	 * Desordena el contenido de la matriz
	 * @param secret la matriz que le paso
	 * @return secret la matriz que devuelvo
	 */
	
	public static String[][] desordenar(String[][] secret) {

		String aux = " ";

		for (int f = 0; f < 4; f++) {
			for (int c = 0; c < 4; c++) {
				int fila = r.nextInt(4);
				int columna = r.nextInt(4);
				aux = secret[fila][columna];
				secret[fila][columna] = secret[f][c];
				secret[f][c] = aux;

			}
		}

		return secret;
	}
	
	/**
	 * El usuario inserta 2 n�meros (fila y columna) para comprobar si esa posici�n es v�lida o no (tapada o destapada)
	 * @param tauler la matriz que le paso
	 * @param secret la matriz que le paso
	 * @return array el array que devuelvo con la posicion v�lida
	 */
	
	public static int[] triarCasella(String[][] tauler, String[][] secret) { 

		System.out.println("Insereix una posicio (fila columna)");
		int fila = sc.nextInt();
		int columna = sc.nextInt();
		int [] array = {fila, columna};
		
		if(tauler[fila][columna].equals("?")) {
			tauler[fila][columna] = secret[fila][columna];
			mostrarTauler(tauler);
			
		}else {
			mostrarTauler(tauler);
			System.out.println("La casella ja est� destapada >:(");
			System.out.println();
			array = triarCasella(tauler, secret);
		}
		
		return array; 
	}
	
	/**
	 * Comprueba si 2 posiciones v�lidas son pareja o no. Si son pareja se quedan destapadas, sino muestra el tablero con su contenido y vuelve a tapar.
	 * @param tauler la matriz que le paso
	 * @param secret la matriz que le paso
	 * @return puntito pra poder hacer un ranking en la funcion partida multijugador
	 */
	
	public static boolean ferJugada(String[][] tauler, String[][] secret) {
		
		int[] array1 = triarCasella(tauler, secret); 
		int[] array2 = triarCasella(tauler, secret); 
		boolean puntito = false;
		
		if(secret[array1[0]][array1[1]].equals(secret[array2[0]][array2[1]])) {
			tauler[array1[0]][array1[1]] = secret[array1[0]][array1[1]]; 
			tauler[array2[0]][array2[1]] = secret[array2[0]][array2[1]];
			System.out.println("PARELLA");
			puntito = true;
			
		} else {
			System.out.println("peepoclown");
			tauler[array1[0]][array1[1]] = "?"; 
			tauler[array2[0]][array2[1]] = "?";
			mostrarTauler(tauler);
		}
		
		return puntito; 
	}
	
	/**
	 * Cuenta cuantas casillas est�n tapadas ("?") 
	 * @param tauler la matriz que le paso
	 * @return caselles - la cantidad de casillas para poder finalizar una partida 
	 */
	
	public static int casellesPendents(String[][] tauler) { 
		
		int caselles = 0;
		
		for (int f = 0; f < 4; f++) {
			for (int c = 0; c < 4; c++) {
				if(tauler[f][c].equals("?")){
					caselles++;
				}
			}
		}
		
		return caselles;
	}
	
	/**
	 * Permite jugar una partida solitaria (1 jugador) Si la cantidad de casillas tapadas es mayor a 0, la partida sigue en curso.
	 * @param tauler la matriz que le paso
	 * @param secret la matriz que le paso
	 */
	
	public static void partidaSolitaria(String[][] tauler, String[][] secret) { 
		
		int caselles = casellesPendents(tauler);
		
		while(caselles > 0) {
			ferJugada(tauler, secret);
			caselles = casellesPendents(tauler);
		}
		
		System.out.println("HAS GUANYAT!!! OLEOLE");
	}
	

	/**
	 * Permite jugar una partida multijugador (2 jugadores)
	 * Cada jugador inserta su nombre y por turnos transcurre la partida.
	 * Finalmente me suma 1 punto por partida donde el jugador haya hecho m�s parejas.
	 * @param tauler la matriz que le paso
	 * @param secret la matriz que le paso
	 */
	
	public static void partidaMultijuadors(String[][] tauler, String[][] secret) { 
		
		sc.nextLine();
		System.out.println("Nom jugador 1: ");
		String jugador1 = sc.nextLine();
		int puntitos1 = 0;
		
		System.out.println("Nom jugador 2: ");
		String jugador2 = sc.nextLine();
		int puntitos2 = 0;
		
		int caselles = casellesPendents(tauler);
		int contador = 0;
		
		while(caselles > 0) {
			if(contador % 2 == 0) { 
				System.out.println("Es el torn de: "+jugador1);
				if(ferJugada(tauler, secret)) { 
					puntitos1++;
				}
				
			}else {
				System.out.println("Es el torn de: "+jugador2);
				if(ferJugada(tauler, secret)) {
					puntitos2++;
				}
				
			}
			
			contador++;
			caselles = casellesPendents(tauler);
		}
		
		if(puntitos1 > puntitos2) { 
			System.out.println("HA GUANYAT "+ jugador1);
			System.out.println();
			
		}else {
			System.out.println("HA GUANYAT "+ jugador2);
			System.out.println();
		}
		
	}
	
}