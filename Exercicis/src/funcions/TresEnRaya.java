package funcions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class TresEnRaya {

	static Scanner sc;

	public static void main(String[] args) {

		sc = new Scanner(System.in);
		ArrayList<String> jugadors = new ArrayList<String>();
		ArrayList<Integer> ranking = new ArrayList<Integer>(Arrays.asList(0, 0, 0, 0));
		boolean exit = false;
		boolean flag = false;
		char[][] tauler = new char[3][3];

		do {

			mostrarMenu();
			int opcio = sc.nextInt();
			sc.nextLine();

			switch (opcio) {
			case 1:
				mostrarAjuda();
				break;

			case 2:
				jugadors = definirJugadors();
				flag = true;
				break;

			case 3:
				if (flag) {
					iniciarMatriu(tauler);
					visualitzarMatriu(tauler);
					jugarPartida(tauler, ranking, jugadors);

				} else {
					System.err.println("No hi ha jugadors definits");
				}

				break;

			case 4:
				if (flag) {
					veureJugadors(jugadors, ranking);
				} else {
					System.err.println("No hi ha jugadors definits");
				}
				break;

			case 0:
				exit = true;
				System.out.println("A10");
				break;

			default:
				System.err.println(opcio + " no es una opcio valida");
			}

		} while (!exit);
	}

	private static void veureJugadors(ArrayList<String> jugadors, ArrayList<Integer> ranking) {

		System.out.println(jugadors.get(0) + " guanyades: " + ranking.get(0));
		System.out.println(jugadors.get(0) + " perdudes: " + ranking.get(3));
		System.out.println(jugadors.get(1) + " guanyades: " + ranking.get(1));
		System.out.println(jugadors.get(1) + " perdudes: " + ranking.get(2));
	}

	private static void visualitzarMatriu(char[][] tauler) {

		for (int f = 0; f < 3; f++) {
			System.out.print(f + " ");
			for (int c = 0; c < 3; c++) {
				System.out.print(tauler[f][c] + " ");
			}
			System.out.println();
		}
		System.out.println("  0 1 2");
	}

	private static void jugarPartida(char[][] tauler, ArrayList<Integer> ranking, ArrayList<String> jugadors) {

		int mequieromorir;
		int contador = 0;
		mequieromorir = comprovarTauler(tauler);

		while (mequieromorir == 3) {

			int[] coords = new int[2];
			demanarCoords(tauler, coords);

			if (contador % 2 == 0) {
				tauler[coords[0]][coords[1]] = 'O';
				contador++;
			} else {
				tauler[coords[0]][coords[1]] = 'X';
				contador++;
			}

			visualitzarMatriu(tauler);
			mequieromorir = comprovarTauler(tauler);
			if (mequieromorir == 1) {
				int aux = ranking.get(0) + 1;
				ranking.set(0, aux);
				aux = ranking.get(2) + 1;
				ranking.set(2, aux);
				System.out.println("Ha guanyat: " + jugadors.get(0));
			}

			if (mequieromorir == 2) {
				int aux = ranking.get(1) + 1;
				ranking.set(1, aux);
				aux = ranking.get(3) + 1;
				ranking.set(3, aux);
				System.out.println("Ha guanyat: " + jugadors.get(1));
			}

		}

	}

	private static int comprovarTauler(char[][] tauler) {

		boolean flag = false;

		for (int f = 0; f < 3; f++) { // horitzontal
			int contador1 = 0;
			int contador2 = 0;

			for (int c = 0; c < 3; c++) {
				if (tauler[f][c] == 'O') {
					contador1++;
				}

				if (tauler[f][c] == 'X') {
					contador2++;
				}

			}

			if (contador1 == 3) {
				return 1;

			} else if (contador2 == 3) {
				return 2;
			}

			contador1 = 0;
			contador2 = 0;
		}

		for (int f = 0; f < 3; f++) { // vertical
			int contador1 = 0;
			int contador2 = 0;

			for (int c = 0; c < 3; c++) {
				if (tauler[c][f] == 'O') {
					contador1++;
				}

				if (tauler[c][f] == 'X') {
					contador2++;
				}

			}

			if (contador1 == 3) {
				return 1;

			} else if (contador2 == 3) {
				return 2;
			}

			contador1 = 0;
			contador2 = 0;
		}

		int contador1 = 0; // diagonal dreta
		int contador2 = 0;

		for (int i = 0; i < tauler.length; i++) {
			if (tauler[i][i] == 'O') {
				contador1++;
			}

			if (tauler[i][i] == 'X') {
				contador2++;
			}
		}

		if (contador1 == 3) {
			return 1;

		} else if (contador2 == 3) {
			return 2;
		}

		contador1 = 0; // diagonal esquerra.
		contador2 = 0;
		int ayudita = 2;

		for (int f = 0; f < 3; f++) {
			if (tauler[f][ayudita] == 'O') {
				contador1++;
			}

			if (tauler[f][ayudita] == 'X') {
				contador2++;
			}

			ayudita--;
		}

		if (contador1 == 3) {
			return 1;

		} else if (contador2 == 3) {
			return 2;
		}
		
		for (int f = 0; f < 3; f++) {
			for (int c = 0; c < 3; c++) {
				if (tauler[f][c] == '�') {
					flag = true;
				}
			}
		}
		
		if(flag) {
			return 3;
			
		} else {
			System.out.println("Empate");
			return 0;
		}
		
	}

	private static void demanarCoords(char[][] tauler, int[] coords) {

		System.out.println("Introdueix una posici� (fila-columna)");
		int fila = sc.nextInt();
		int columna = sc.nextInt();

		if (tauler[fila][columna] == '�') {
			coords[0] = fila;
			coords[1] = columna;

		} else {
			System.out.println("La posicio ja est� destapada");
			demanarCoords(tauler, coords);
		}

	}

	private static void iniciarMatriu(char[][] tauler) {

		for (int f = 0; f < 3; f++) {
			for (int c = 0; c < 3; c++) {
				tauler[f][c] = '�';
			}
		}
	}

	private static ArrayList<String> definirJugadors() {

		System.out.println("Introdueix el nom del jugador 1");
		String jugador1 = sc.nextLine();
		System.out.println("Introdueix el nom del jugador 2");
		String jugador2 = sc.nextLine();

		if (jugador1.equals(jugador2)) {
			System.err.println("Els 2 jugadors no poden tenir el mateix nom");
			definirJugadors();
		}

		ArrayList<String> jugadors = new ArrayList<String>(Arrays.asList(jugador1, jugador2));

		return jugadors;

	}

	private static void mostrarAjuda() {

		System.out.println("El primer que fagi 3 en ratlla ganya.");

	}

	private static void mostrarMenu() {

		System.out.println("Si us plau llista una opcio valida:");
		System.out.println();
		System.out.println("1- Mostrar ajuda");
		System.out.println("2- Definir Jugadors");
		System.out.println("3- Jugar partida");
		System.out.println("4- Veure jugadors");
		System.out.println("0- Sortir");

	}

}
