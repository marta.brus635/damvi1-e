package mokepon;

public class MokemonCapturat extends Mokemon {

	String nickname;

	String trainerName;

	int hapiness;
	
	Item item;
	
	@Override
	public String toString() {
		return "MokemonCapturat [nickname=" + nickname + ", trainerName=" + trainerName + ", hapiness=" + hapiness
				+ "]";
	}

	public MokemonCapturat(String name, Types type) {

		super(name, type);

		this.nickname = name;

		this.trainerName = "Mana";

		this.hapiness = 50;
	}

	public MokemonCapturat() {
		
		super();
		this.nickname = "Not Defined";
		this.trainerName = "Not Defined";
		this.hapiness = 0;
	}
	
	public MokemonCapturat(String name, int level) {

		super(name, level);
		this.nickname = "Not Defined";
		this.trainerName = "Not Defined";
		this.hapiness = 0;
	}
	
	public MokemonCapturat(String name, int lvl, int max_hp, int atk, int def, int spd) {
		
		super(name, lvl, max_hp, atk, def, spd);
		this.nickname = "Not Defined";
		this.trainerName = "Not Defined";
		this.hapiness = 0;
	}
	
	public MokemonCapturat(Mokemon mok, String nickname, String trainerName) {
		
		super(mok.name, mok.type);
		this.nickname = nickname;
		this.trainerName = trainerName;
		this.hapiness = 50;
	}
	
	public void pet() {
		
		if(this.hapiness < 100) {
			this.hapiness += 10;
			System.out.println("Felicitat de "+this.name+" +10");
			
		} else {
			System.out.println("La felicitat ja est� al tope!");
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + hapiness;
		result = prime * result + ((item == null) ? 0 : item.hashCode());
		result = prime * result + ((nickname == null) ? 0 : nickname.hashCode());
		result = prime * result + ((trainerName == null) ? 0 : trainerName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MokemonCapturat other = (MokemonCapturat) obj;
		if (hapiness != other.hapiness)
			return false;
		if (item == null) {
			if (other.item != null)
				return false;
		} else if (!item.equals(other.item))
			return false;
		if (nickname == null) {
			if (other.nickname != null)
				return false;
		} else if (!nickname.equals(other.nickname))
			return false;
		if (trainerName == null) {
			if (other.trainerName != null)
				return false;
		} else if (!trainerName.equals(other.trainerName))
			return false;
		return true;
	}

	public void atacar(Mokemon atacat, int num_atac) {
		double dmg = (((((((2*this.lvl)/5)+2) * this.Attacks.get(num_atac).power * (this.atk / atacat.def)) /50) + 2) * Modifier(this.Attacks.get(num_atac).type, atacat.type));
		
		if(this.hapiness >= 50) {
			dmg *= 1.2;
		} else {
			dmg *= 0.8;
		}
		
		System.out.println("El atac ha sigut de: "+dmg);
		atacat.current_hp -= (int) dmg;
		
		atacat.fainted = fainted(atacat);
		System.out.println("Al pokemon atacat li queden: "+atacat.current_hp+" hp");
	}
	
	public void useItem() {
		this.item.use(this);
	}
}