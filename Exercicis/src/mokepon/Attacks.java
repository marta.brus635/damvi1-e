package mokepon;

public class Attacks {

	String name;

	double power;

	Types type;

	int max_movements;

	int current_movements;
	
	@Override
	public String toString() {
		return "Attacks [name=" + name + ", power=" + power + ", type=" + type + ", max_movements=" + max_movements
				+ ", current_movements=" + current_movements + "]";
	}

	public Attacks(String name, int power, Types type, int max_movement) {

		this.name = name;

		if (power < 10) {
			this.power = 10;
		} else if (power > 100) {
			this.power = 100;
		} else {
			this.power = power;
		}

		this.type = type;
		this.max_movements = max_movement;
		this.current_movements = max_movement;
	}

	public Attacks(String name, Types type) {

		this.name = name;
		this.type = type;
		this.power = 10;
		this.max_movements = 10;

	}

}
