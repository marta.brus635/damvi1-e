package mokepon;

import java.util.ArrayList;
import java.util.Random;

public class Mokemon {

	Random r;
	
	String name;
	
	int lvl;
	
	int atk;
	
	int def;
	
	int spd;
	
	int xp;
	
	int max_hp;
	
	int current_hp;

	Types type;
	
	ArrayList<Attacks> Attacks = new ArrayList<Attacks>(2);
	
	boolean fainted;

	Equipment equippedItem;
	
	Genres genre;
	
	@Override
	public String toString() {
		return "Mokemon [r=" + r + ", name=" + name + ", lvl=" + lvl + ", atk=" + atk + ", def=" + def + ", spd=" + spd
				+ ", xp=" + xp + ", max_hp=" + max_hp + ", current_hp=" + current_hp + ", type=" + type + ", Attacks="
				+ Attacks + ", fainted=" + fainted + "]";
	}

	public void sayname() {
		
		System.out.println("-----------------------");
		System.out.println("Name: "+this.name);
		System.out.println("Level: "+this.lvl);
		System.out.println("Experience: "+this.xp);
		System.out.println("Attack: "+this.atk);
		System.out.println("Defence: "+this.def);
		System.out.println("Speed: "+this.spd);
		System.out.println("Max Hp: "+this.max_hp);
		System.out.println("Type: "+this.type);
		System.out.println("Genre: "+this.genre);
		System.out.println("-----------------------");
		
	}
	
	public void giveExperience(int given_xp) {
		
		this.xp += given_xp;
		
		while(this.xp >= 100) {
			this.xp -= 100;
			levelup();
		}
	}
	
	public void levelup() {
		
		r = new Random();
		
		this.lvl++;
		this.max_hp += r.nextInt(6);
		this.atk += r.nextInt(3);
		this.def += r.nextInt(3);
		this.spd += r.nextInt(3);
	}

	public Mokemon() {
		
		this.name = "Not defined";
		this.lvl = 1;
		this.atk = 1;
		this.def = 1;
		this.spd = 1;
		this.max_hp = 10;
	}
	
	public Mokemon(String name, Types type) {
		
		this.name = name;
		this.lvl = 1;
		this.atk = 1;
		this.def = 1;
		this.spd = 1;
		this.max_hp = 10;
		this.type = type;
		this.genre = randomGenre();
	}
	
	public Mokemon(String name, int level) {
		
		this.name = name;
		for (int i = 1; i < level; i++) {
			this.levelup();
		}
		
		this.current_hp = this.max_hp;
		this.genre = randomGenre();
	}
	
	public Mokemon(String name, int lvl, int max_hp, int atk, int def, int spd) {
		this.name = name;
		this.lvl = lvl;
		this.max_hp = max_hp;
		this.atk = atk;
		this.def = def;
		this.spd = spd;
		this.genre = randomGenre();
	}
	
	public void addAttack(Attacks aaa) {
		this.Attacks.add(aaa);
	}
	
	
	public double Modifier(Types attack, Types def) {
		if(attack == Types.FIRE && def == Types.WATER
				|| attack == Types.WATER && def == Types.PLANT
				|| attack == Types.PLANT && def == Types.FIRE) {
			
			return 0.5;
			
		} else if(attack == Types.WATER && def == Types.FIRE
				|| attack == Types.FIRE && def == Types.PLANT
				|| attack == Types.PLANT && def == Types.WATER) {
			
			return 2;
			
		} else {
			return 1;
		}
		
	}
	
	public void atacar(Mokemon atacat, int num_atac) {
		double dmg = (((((((2*this.lvl)/5)+2) * this.Attacks.get(num_atac).power * (this.atk / atacat.def)) /50) + 2) * Modifier(this.Attacks.get(num_atac).type, atacat.type));
		
		System.out.println("El atac ha sigut de: "+dmg);
		atacat.current_hp -= (int) dmg;
		
		atacat.fainted = fainted(atacat);
		System.out.println("Al pokemon atacat li queden: "+atacat.current_hp+" hp");
	}

	public boolean fainted(Mokemon atacat) {
		if(atacat.current_hp <= 0) {
			System.out.println("Pokemon debilitat!");
			atacat.current_hp = 0;
			return true;
			
		} else {
			return false;
		}
	}
	
	public void healing() {
		this.current_hp = this.max_hp;
		this.fainted = false;
	}
	
	public MokemonCapturat capturar(String trainerName, String nickname) {
		
		if(!(this instanceof MokemonCapturat)) {
			MokemonCapturat algo = new MokemonCapturat(this, nickname, trainerName);
			return algo;
			
		} else {
			System.out.println("No pots capturar un Mokemon que ja esta capturat");
			return (MokemonCapturat) this;
		}
	}
	
	public Genres randomGenre() {
		
		int num = r.nextInt(2);
		
		if(num == 0) {
			return Genres.Male;
		} else {
			return Genres.female;
		}
	}
	
	/*public Egg reproduccio(Mokemon mokemon1, Mokemon mokemon2) {
		Egg egg;
		boolean flag1, flag2, flag3;
		
		if(mokemon1.genre == Genres.female && mokemon2.genre == Genres.Male
				|| mokemon1.genre == Genres.Male && mokemon2.genre == Genres.female) {
			flag1 = true;
		}
		
		if(mokemon1.type ==  mokemon2.type) {
			flag2 = true;
		}
		
		if(!mokemon1.fainted && !mokemon2.fainted) {
			flag3 = true;
		}
		
		if(flag1 && flag2 && flag3) {
			
			int num = r.nextInt(2);
			
			if(num == 1) {
				String specie = mokemon1.name;
			} else {
				String specie = mokemon2.name;
			}
			egg = new Egg(specie, mokemon1.type);
			return egg;
		} else {
			return null;
		}
	}*/
	
}
