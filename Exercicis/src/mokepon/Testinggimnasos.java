package mokepon;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

public class Testinggimnasos {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		try {
			// path al fitxer
			File f = new File("resources/text/gimnasos.txt");
			FileWriter fw;
			fw = new FileWriter(f, true);
			BufferedWriter bw = new BufferedWriter(fw);
			System.out.println("Writer Carregat Correctament");
			
			// FUNCIONS IMPORTANTS DEL BUFFEREDWRITER
			// append. Escriu al buffer intern. Sense salts de linea. No escriu al fitxer
			// fins que fas un flush
			bw.append("gimnasio1;");
			bw.append("ciudad1;");
			bw.append("lider1;");
			bw.append("0\n");
			bw.append("gimnasio2;");
			bw.append("ciudad2;");
			bw.append("lider2;");
			bw.append("0\n");
			
			// Guarda tots els canvis que has fet al buffer intern. Escriu a fitxer
			bw.flush();
			bw.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("El fitxer no existeix");
			e.printStackTrace();
			
		} catch (IOException e) {
			System.out.println("Excepci� general d'escriptura");
			e.printStackTrace();
		}
		
	}

}
