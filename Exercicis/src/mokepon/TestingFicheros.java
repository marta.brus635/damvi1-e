package mokepon;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class TestingFicheros {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

        boolean bucle = true;

        while (bucle) {

            menu();

            int opcion = sc.nextInt();
            sc.nextLine();

            switch (opcion) {
            case 1:
            	System.out.println("Gym: ");
                String Gym = sc.nextLine();

                System.out.println("Ciudad: ");
                String Ciudad = sc.nextLine();

                System.out.println("Lider: ");
                String Lider = sc.nextLine();

                A�adirGimnasio(Gym, Ciudad, Lider);
                System.out.println();
                break;
                
            case 2:
            	mostrarGyms();
                break;
                
            case 3:
            	System.out.println("Escriu el nom del gimnas");
            	String nombre = sc.nextLine();
            	cercaLider(nombre);
                break;
                
            case 4:
            	System.out.println("Insereix un numero");
            	int numero = sc.nextInt();
            	sc.nextLine();
            	invictes(numero);
                break;
                
            case 5:
            	copiaSeguretat("resources/text/gimnasos.txt", "resources/text/copiaGimnasos.txt");
            	break;
            	
            case 6:
            	System.out.println("Insereix gimnas: ");
            	String gimnas = sc.nextLine();
            	System.out.println("Insereix Lider: ");
            	String lider = sc.nextLine();
            	canviLider(gimnas, lider);
            	break;
            	
            case 7:
            	System.out.println("Insereix gimnas: ");
            	String gimnasio = sc.nextLine();
            	System.out.println("Insereix entrenador: ");
            	String entrenador = sc.nextLine();
            	afegeixEntrenador(gimnasio, entrenador);
            	break;
            	
            case 8:
            	System.out.println("Insereix gimnas: ");
            	String cosita = sc.nextLine();
            	esborraGimnas(cosita);
            	break;
            
            case 9:
            	System.out.println("Insereix gimnas: ");
            	String gym = sc.nextLine();
            	consultaEntrenadors(gym);
            	break;
            	
            case 0:
            	bucle = false;
            	System.out.println("a10");
            	break;
            	
            default:
                System.out.println("Manito tas equivocao");
            }
        }

	}

	private static void consultaEntrenadors(String gym) {
		try {
			File f = new File("resources/text/gimnasos.txt");
	        FileReader fr;
	        fr = new FileReader(f);
	        BufferedReader br = new BufferedReader(fr);            
	        
	        while(br.ready()) {
	        	String linea = br.readLine();
	        	String[] aux = linea.split(";");
	        	
	        	if(aux[0].equals(gym)) {
	        		for (int i = 4; i < aux.length; i++) {
						System.out.println(aux[i]);
					}
	        	}
            }
	        
            br.close();
                 
		} catch (FileNotFoundException e) {
            System.out.println("El fitxer no existeix");
            e.printStackTrace();
            
        } catch (IOException e) {
            System.out.println("Excepci� general de lectura");
            e.printStackTrace();
        }
		
	}

	private static void esborraGimnas(String gimnas) {
		try {
			File f = new File("resources/text/gimnasos.txt");
	        FileReader fr;
	        fr = new FileReader(f);
	        BufferedReader br = new BufferedReader(fr);            
	        
	        File f2 = new File("resources/text/gimnasosNOU.txt");
	        FileWriter fw;
	        fw = new FileWriter(f2);
	        BufferedWriter bw = new BufferedWriter(fw); 
	        
	        while(br.ready()) {
	        	String linea = br.readLine();
	        	String[] aux = linea.split(";");
	        	
	        	if(!aux[0].equals(gimnas)) {
	        		bw.append(linea+"\n");
	        	}
            }
	        
	        bw.close();
            br.close();
            System.out.println(f.delete());
            System.out.println(f2.renameTo(f));
                 
		} catch (FileNotFoundException e) {
            System.out.println("El fitxer no existeix");
            e.printStackTrace();
            
        } catch (IOException e) {
            System.out.println("Excepci� general de lectura");
            e.printStackTrace();
        }
		
	}

	private static void afegeixEntrenador(String gimnasio, String entrenador) {
		try {
			File f = new File("resources/text/gimnasos.txt");
	        FileReader fr;
	        fr = new FileReader(f);
	        BufferedReader br = new BufferedReader(fr);            
	        
	        File f2 = new File("resources/text/gimnasosNOU.txt");
	        FileWriter fw;
	        fw = new FileWriter(f2);
	        BufferedWriter bw = new BufferedWriter(fw); 
	        
	        while(br.ready()) {
	        	String linea = br.readLine();
	        	String[] aux = linea.split(";");
	        	
	        	if(aux[0].equals(gimnasio)) {
	        		int num = Integer.parseInt(aux[3]);
	        		num++;
	        		aux[3] = String.valueOf(num);
	        		for (int i = 0; i < aux.length; i++) {
						bw.append(aux[i]+";");
					}
	            	bw.append(entrenador+"\n");
	        	
	        	}else {
	        		bw.append(linea+"\n");
	        	}
            }
	        
	        bw.close();
            br.close();
            System.out.println(f.delete());
            System.out.println(f2.renameTo(f));
                 
		} catch (FileNotFoundException e) {
            System.out.println("El fitxer no existeix");
            e.printStackTrace();
            
        } catch (IOException e) {
            System.out.println("Excepci� general de lectura");
            e.printStackTrace();
        }
		
	}

	private static void invictes(int numero) {
		// TODO Auto-generated method stub
		try {
            File f = new File("resources/text/gimnasos.txt");
            FileReader fr;
            fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);            
            System.out.println("Reader Carregat Correctament");          
            
            while(br.ready()) {
            	String[] aux = br.readLine().split(";");
            	int aux2 = Integer.parseInt(aux[3]);
            	
            	if(aux2 < numero) {
            		System.out.println(aux[0]);
            	}
            }
            
        } catch (FileNotFoundException e) {
            System.out.println("El fitxer no existeix");
            e.printStackTrace();
            
        } catch (IOException e) {
            System.out.println("Excepci� general de lectura");
            e.printStackTrace();
        }
	}

	private static void cercaLider(String gimnasio) {
		// TODO Auto-generated method stub
		try {
            File f = new File("resources/text/gimnasos.txt");
            FileReader fr;
            fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);            
            System.out.println("Reader Carregat Correctament");          
            
            while(br.ready()) {
            	String[] aux = br.readLine().split(";");
            	if(aux[0].equals(gimnasio)) {
            		System.out.println(aux[2]);
            	}
            }
            
        } catch (FileNotFoundException e) {
            System.out.println("El fitxer no existeix");
            e.printStackTrace();
            
        } catch (IOException e) {
            System.out.println("Excepci� general de lectura");
            e.printStackTrace();
        }
	}

	private static void mostrarGyms() {
		// TODO Auto-generated method stub
		try {
            File f = new File("resources/text/gimnasos.txt");
            FileReader fr;
            fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);            
            System.out.println("Reader Carregat Correctament");          
            
            while(br.ready()) {
            	String[] aux = br.readLine().split(";");
            	System.out.println(aux[0]);
            }
            
            br.close();
            
        } catch (FileNotFoundException e) {
            System.out.println("El fitxer no existeix");
            e.printStackTrace();
            
        } catch (IOException e) {
            System.out.println("Excepci� general de lectura");
            e.printStackTrace();
        }

	}

	private static void A�adirGimnasio(String gym, String ciudad, String lider) {
		// TODO Auto-generated method stub
		 try {

             File f = new File("resources/text/gimnasos.txt");

             FileWriter fw;
             fw = new FileWriter(f, true);

             BufferedWriter bw = new BufferedWriter(fw);

             bw.append(gym+";");
             bw.append(ciudad+";");
             bw.append(lider+";");
             bw.append("0\n");

             bw.flush();
             bw.close();

         } catch (FileNotFoundException e) {
             System.out.println("El fitxer no existeix");
             e.printStackTrace();
         } catch (IOException e) {
             System.out.println("Excepci� general d'escriptura");
             e.printStackTrace();
         }

	}
	
	private static void copiaSeguretat(String path1, String path2) {
		try {
            File f = new File(path1);
            FileReader fr;
            fr = new FileReader(f);
            BufferedReader br = new BufferedReader(fr);            
            
            File f2 = new File(path2);
            FileWriter fw;
            fw = new FileWriter(f2);
            BufferedWriter bw = new BufferedWriter(fw); 
            
            while(br.ready()) {
            	String aux = br.readLine();
            	bw.append(aux+"\n");
            }
            
            bw.flush();
            bw.close();
            br.close();
            
        } catch (FileNotFoundException e) {
            System.out.println("El fitxer no existeix");
            e.printStackTrace();
            
        } catch (IOException e) {
            System.out.println("Excepci� general de lectura");
            e.printStackTrace();
        }
	}
	
	private static void canviLider(String nomGimnas, String nouLider) {
		
		try {
			File f = new File("resources/text/gimnasos.txt");
	        FileReader fr;
	        fr = new FileReader(f);
	        BufferedReader br = new BufferedReader(fr);            
	        
	        File f2 = new File("resources/text/gimnasosNOU.txt");
	        FileWriter fw;
	        fw = new FileWriter(f2);
	        BufferedWriter bw = new BufferedWriter(fw); 
	        
	        while(br.ready()) {
	        	String[] aux = br.readLine().split(";");
            	if(aux[0].equals(nomGimnas)) {
            		aux[2] = nouLider;
            	}
            	
            	for (int i = 0; i < aux.length-1; i++) {
            		bw.append(aux[i]+";");
				}
            	bw.append(aux[aux.length-1]+"\n");
            }
	        
	        bw.close();
            br.close();
            System.out.println(f.delete());
            System.out.println(f2.renameTo(f));
                 
		} catch (FileNotFoundException e) {
            System.out.println("El fitxer no existeix");
            e.printStackTrace();
            
        } catch (IOException e) {
            System.out.println("Excepci� general de lectura");
            e.printStackTrace();
        }
	}

	private static void menu() {
		System.out.println();
        System.out.println("---------");
        System.out.println("1. - A�adir Entry");
        System.out.println("2. - Ver Gimnasios");
        System.out.println("3. - Buscar Lider");
        System.out.println("4. - Invictos");
        System.out.println("5. - Copia de seguretat");
        System.out.println("6. - Canvi de lider");
        System.out.println("7. - Afegir entrenador al gimnas");
        System.out.println("8. - Esborrar gimnas");
        System.out.println("9. - Consultar entrenadors");
        System.out.println("0. - Sortir");
        System.out.println("---------");
	}
}
