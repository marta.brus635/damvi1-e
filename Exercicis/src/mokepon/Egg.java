package mokepon;

import java.util.Random;

public class Egg {

	Random r;
	
	private String species;
	
	private Types type;
	
	private int passesRestants;
	
	public Egg(String species, Types type){
		
		this.species = species;
		this.type = type;
		this.passesRestants = r.nextInt(5)+5;
	}
	
	public void walk() {
		
		this.passesRestants--;
		
		if(this.passesRestants == 0) {
			eclosionar();
		}
	}

	private void eclosionar() {
		Mokemon aa = new Mokemon(this.species, this.type);
	}
	
}
