package recursivitat;

public class Exercici4 {

	public static void main(String[] args) {
		
		int num = 0;
		int contador = 100;
		int resultat = suma(num, contador);
		
		System.out.println(resultat);
	}

	private static int suma(int num, int contador) {
		
		if(contador == 0) {
			return 0;
		}else {
			if(num % 2 == 0) {
				return num + suma(num+1, contador-1);			
			} else {
				return suma(num+1, contador);
			}
		}
	}

}
