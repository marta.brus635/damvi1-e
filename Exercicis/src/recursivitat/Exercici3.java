package recursivitat;

import java.util.Scanner;

public class Exercici3 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Num inicial");
		int num = sc.nextInt();
		System.out.println("Num a restar");
		int numRestar = sc.nextInt();
		
		int resultat = Resta(num, numRestar);
		System.out.println(resultat);
	}

	private static int Resta(int num, int numRestar) {
		
		if (num - numRestar == 0) {
			return 1;
			
		}else if(num - numRestar < 0) {
			return 0;
			
		}else {
			return 1 + Resta(num-numRestar, numRestar);
		}
	}

}
