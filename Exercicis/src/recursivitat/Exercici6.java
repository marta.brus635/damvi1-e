package recursivitat;

import java.util.Scanner;

public class Exercici6 {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		
		System.out.println("Insereix la longitud del vector: ");
		int longitud = sc.nextInt();
		int[] vector = new int[longitud];
		
		for (int i = 0; i < longitud; i++) {
			vector[i] = sc.nextInt();
		}
	}

}
