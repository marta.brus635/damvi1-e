package recursivitat;

import java.util.Scanner;

public class Exercici5 {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Insereix un numero");
		int num = sc.nextInt();
		
		int resultat = sumaXifres(num);
		
		System.out.println("La suma dels digits es: "+resultat);
	}

	private static int sumaXifres(int num) {
		
		if(num == 0) {
			return 0;
		} else {
			return num%10 + sumaXifres(num/10);
 		}
		
	}

}
