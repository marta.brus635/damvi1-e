package matrius;

import java.util.Scanner;

public class PanBimbo {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int longitud = sc.nextInt();

		iniciar(longitud);

	}

	public static char[][] iniciar(int longitud) {
		char[][] matriu = new char[longitud][longitud];

		for (int f = 0; f < longitud; f++) {
			for (int c = 0; c < longitud; c++) {
				if (f == 0 || f == longitud - 1) {
					matriu[f][c] = ' ';
					System.out.print(matriu[f][c]);

				} else if (c == 0 || c == longitud - 1) {
					matriu[f][c] = ' ';
					System.out.print(matriu[f][c]);

				} else
					matriu[f][c] = 'X';
				System.out.print(matriu[f][c]);
			}
			System.out.println();
		}
		
		return matriu;
	}

}
