package uf3_examen;

public class KoopalingTeam extends Pilot implements Malote{

	public KoopalingTeam(String nom) {
		super(nom);
	}

	@Override
	public void agafarObjecte(Objecte ob) {
		if(this.elMeuObjecte == null) {
			this.elMeuObjecte = ob;
			
		}else {
			if(this.elMeuObjecte instanceof Neumatic && ob instanceof Neumatic) {
				if(((Neumatic)ob).superNeumatic) {
					this.elMeuObjecte = ob;
				}
				
			}else if(!(this.elMeuObjecte instanceof Neumatic) && ob instanceof Neumatic) {
				this.elMeuObjecte = ob;
			}
		}
			
	}

	@Override
	public String cridar() {
		return "SIIIIIIIIII";
	}

	@Override
	public String toString() {
		return "KoopalingTeam [nom=" + nom + ", posicio=" + posicio + ", elMeuObjecte=" + elMeuObjecte + "]";
	}

	@Override
	public void xocar(Pilot p) {
		
		if(this.equals(p)) {
			return;
			
		}else if(this.elMeuObjecte == null){
			this.posicio = 0;
			return;
			
		}else if(!(this.elMeuObjecte == null) && p.elMeuObjecte == null) {
			p.posicio = 0;
			return;
			
		}else if(!(this.elMeuObjecte == null) && !(p.elMeuObjecte == null)) {
			if(this.elMeuObjecte instanceof Platan) {
				p.posicio = 0;
				this.elMeuObjecte = null;
				return;
				
			}else if(this.elMeuObjecte instanceof Neumatic) {
				this.elMeuObjecte = null;
				
			}else if(this.elMeuObjecte instanceof Moneda && p.elMeuObjecte instanceof Moneda) {
				((Moneda)this.elMeuObjecte).valor = ((Moneda)p.elMeuObjecte).valor;
				((Moneda)p.elMeuObjecte).valor = 0;
				return;
			}
		}
		
	}
	
	
	
}
