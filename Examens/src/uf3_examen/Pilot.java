package uf3_examen;

public abstract class Pilot {

	public String nom;
	public int posicio;
	public Objecte elMeuObjecte;
	
	public Pilot(String nom) {
		this.nom = nom;
		this.posicio = 0;
		this.elMeuObjecte = null;
	}

	@Override
	public String toString() {
		return "Pilot [nom=" + nom + ", posicio=" + posicio + ", elMeuObjecte=" + elMeuObjecte + "]";
	}
	
	public abstract void agafarObjecte(Objecte ob);
	
	public abstract String cridar ();
	
	public boolean avansar(Circuit c, int increment) {
		this.posicio += increment;
		
		if(this.posicio >= 50) {
			this.cridar();
			return false;
			
		} else {
			if(!(c.caselles[this.posicio] == null)) {
				this.agafarObjecte(c.caselles[this.posicio]);
				
				if(c.caselles[this.posicio] instanceof Moneda) {
					c.caselles[this.posicio] = null;
				}
			}
			
			return true;
		}
	}
	
	public boolean equals(Object obj) {
		
		if(this.equals(obj)) {
			return true;
			
		} else if(obj == null) {
			return false;
			
		}else if(this.getClass().equals(obj.getClass())) {
			return true;
			
		} else {
			return false;
		}
	}
	 
}
