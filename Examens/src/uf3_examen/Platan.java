package uf3_examen;

public class Platan extends Objecte {

	static int totalPlatans = 0;
	public int numPlatan;
	
	public Platan() {
		this.totalPlatans++;
		this.numPlatan = this.totalPlatans;
		this.TipusObjecte = TipusObjecte.ATAC;
		this.prioritari = false;
		
	}
	
	@Override
	public void llen�ar() {
		System.out.println("Toma platanaco");
	}

	@Override
	public String toString() {
		return "Platan -> numPlatan=" + numPlatan + ", prioritari=" + prioritari + ", TipusObjecte=" + TipusObjecte;
	}
}
