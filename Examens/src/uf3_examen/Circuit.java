package uf3_examen;

import java.util.Random;

public class Circuit {

	public Objecte[] caselles;
	Random r;
	
	public Circuit(int nObjectes){
		r = new Random();
		this.caselles = new Objecte[50];
		
		for (int i = 0; i < caselles.length; i++) {
			caselles[i] = null;
		}
		
		if(nObjectes > 20) {
			nObjectes = 20;
		}
		
		for (int i = 0; i < nObjectes; i++) {
			int c = r.nextInt(50);
			
			if(caselles[c] == null) {
				int t = r.nextInt(3);
				
				if(t == 0) {
					Moneda monedita = new Moneda(c);
					caselles[c] = monedita;
					
				}else if(t == 1) {
					Platan platanito = new Platan();
					caselles[c] = platanito;
					
				} else if(t == 2) {
					Neumatic neumatiquito = new Neumatic();
					caselles[c] = neumatiquito;
				}
			}
		}
		
	}
	
	public void visualitzar() {
		for (int i = 0; i < caselles.length; i++) {
			System.out.print("["+caselles[i]+"]");
		}
		System.out.println();
	}
}
