package uf3_examen;

public class Testmain {

	public static void main(String[] args) {
		
		Circuit circuit1 = new Circuit(20);
		
		circuit1.visualitzar();
		
		MarioTeam equip1 = new MarioTeam("Equip1");
		
		System.out.println(equip1.elMeuObjecte);
		while(equip1.avansar(circuit1, 1)) {
			
		}
		System.out.println(equip1.elMeuObjecte);
		
		System.out.println();
		
		
		Circuit circuit2 = new Circuit(20);
		
		circuit2.visualitzar();
		
		KoopalingTeam equip2 = new KoopalingTeam("Equip2");
		
		System.out.println(equip2.elMeuObjecte);
		while(equip2.avansar(circuit2, 1)) {
			
		}
		System.out.println(equip2.elMeuObjecte);
	}

}
