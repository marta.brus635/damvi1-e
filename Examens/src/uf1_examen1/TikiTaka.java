package uf1_examen1;

import java.util.Scanner;

public class TikiTaka {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		String jugadors = sc.nextLine();
		int longitud = jugadors.length();
		boolean flag = false;
		
		for (int i = 2; i < longitud; i++) {
			char primera = jugadors.charAt(i);
			char tercera = jugadors.charAt(i-2);
			
			if(primera == tercera) {
				flag = true;
			}
			
		}
		
		if(flag) {
			System.out.println("SI");
		}else{
			System.out.println("NO");
		}
	}

}
