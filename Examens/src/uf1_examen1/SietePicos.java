package uf1_examen1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class SietePicos {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		int numeritos = sc.nextInt();
		ArrayList<Integer> llista = new ArrayList<Integer>();
		
		for (int i = 0; i < numeritos; i++) {
			llista.add(sc.nextInt());
		}
		
		int max = Collections.max(llista);
		int numPicos = Collections.frequency(llista, max);
		
		System.out.println(numPicos);
	}

}
