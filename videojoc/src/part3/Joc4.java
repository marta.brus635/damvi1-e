package part3;

import java.util.Set;

import Core.Field;
import Core.Window;
import part1.Roca;

public class Joc4 {

	// declaracio field
	static Field f = new Field();
	// declaracio Window
	static Window w = new Window(f, 1500, 900);
	
	public static void main(String[] args) throws InterruptedException {
		f.background = "resources/suelito.png";
		Projectil projectil = new Projectil("bala", 0, 0, 0, 0, 0, "resources/espadita.png", f);
		Personatge pj1 = new Personatge("Link", 725, 425, 775, 475, 0, "resources/Link1.gif", f, projectil);
		Roca rocaAr = new Roca("Roca", -1, -1, 1500, 0, 0, "resources/rock1.png", f, 0);
		Roca rocaD = new Roca("Roca", 1470, 0, 1501, 900, 0, "resources/rock1.png", f, 0);
		Roca rocaAb = new Roca("Roca", 0, 862, 1500, 901, 0, "resources/rock1.png", f, 0);
		Roca rocaI = new Roca("Roca", -1, 0, 0, 901, 0, "resources/rock1.png", f, 0);
		
		
		boolean sortir = false;

		while (!sortir) {
			f.draw();
			Thread.sleep(30);
			Set<Character> keys = input();
			pj1.movimentH(keys);
			pj1.movimentV(keys);
			Set<Character> keysd = inputd();
			
			if (keysd.contains(' ')) {
				pj1.disparar();
			}

		}
	}

	private static Set<Character> input() {
		return w.getPressedKeys();
	}

	private static Set<Character> inputd() {
		return w.getKeysDown();
	}

}
