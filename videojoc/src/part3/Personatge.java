package part3;

import java.util.Set;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import part1.Roca;

public class Personatge extends PhysicBody{

	boolean aterra = false;
	Projectil p;
	
	public Personatge(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, Projectil p) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.p = p;
		
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {

	}

	public void movimentH(Set<Character> ewe) {
		
		if(ewe.contains('d')) {
			this.setVelocity(6, this.velocity[1]);
			
		}else if(ewe.contains('a')) {
			this.setVelocity(-6, this.velocity[1]);
			
		}else {
			this.setVelocity(0, 0);
		}
	}
	
	public void movimentV(Set<Character> ewe) {
		
		if(ewe.contains('w')) {
			this.setVelocity(this.velocity[0], -6);
			
		}else if(ewe.contains('s')) {
			this.setVelocity(this.velocity[0], 6);
			
		}else {
			this.setVelocity(this.velocity[0], 0);
		}
	}
	
	public void disparar() {
		this.p = new Projectil("espaditwww a a", this.x1+50, this.y1+10, this.x2+50, this.y2-10, this.angle, "resources/espadita.png", this.f);
		this.p.setVelocity(6, 0);
	}

	

}
