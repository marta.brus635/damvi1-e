package part2;

import Core.Field;
import Core.Window;
import part1.Roca;

public class Joc3 {

	// declaracio field
	static Field f = new Field();
	// declaracio Window
	static Window w = new Window(f);
	
	public static void main(String[] args) throws InterruptedException {
		Personatge roca = new Personatge("Roca", 50, 600, 180, 700, 0, "resources/Link1.gif", f);
		Roca roca1 = new Roca("roca", 0, 0, 2000, 50, 0, "resources/rock1.png", f, 50);
		Roca roca2 = new Roca("roca", 0, 900, 2000, 1000, 0, "resources/rock1.png", f, 50);
		boolean sortir = false;

		while (!sortir) {
			f.draw();
			Thread.sleep(30);
			input(roca);
			
			/*
			 * 	if(roca.accionsDisponibles == 0) {
					roca.delete();
				}
			 */

		}
	}

	private static void input(Personatge uwu) {
		
		if(w.getPressedKeys().contains('d')) {
			uwu.moviment(Input2.DRETA);
		}
		

		if(w.getPressedKeys().contains('a')) {
			uwu.moviment(Input2.ESQUERRA);
		}
		

		if(w.getKeysDown().contains('w')) {
			uwu.moviment(Input2.SALT);
		}
		
	}
}
