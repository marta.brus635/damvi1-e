package part2;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import part1.Roca;

public class Personatge extends PhysicBody{

	boolean aterra = false;
	int doblesalto = 0;
	
	public Personatge(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		this.setConstantForce(0, 0.2);
		
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		if(sprite instanceof Roca) {
			this.aterra = true;
			this.doblesalto = 2;
		}
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		if(sprite instanceof Roca) {
			this.aterra = false;
		}

	}

	public void moviment(Input2 in) {
		
		if(in == Input2.DRETA) {
			this.setVelocity(1, this.velocity[1]);
		}
		
		if(in == Input2.ESQUERRA) {
			this.setVelocity(-1, this.velocity[1]);
		}
		
		if(in == Input2.SALT && this.doblesalto > 0) {
			this.setForce(0, -2);
			this.doblesalto--;
		}
		
	}

	

}
