package part1;

import Core.Field;
import Core.Sprite;

public class Roca extends Sprite{

	public int accionsDisponibles;
	public static int comptador = 0;
	private int id;
	
	public Roca(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, int accions) {
		
		super(name, x1, y1, x2, y2, angle, path, f);
		this.comptador++;
		this.id = this.comptador;
		this.name += this.id;
		this.accionsDisponibles = accions;
	}
	
	public Roca(int x1, int y1, int x2, int y2, Field f, int accions) {
		
		super("roca", x1, y1, x2, y2, 0, "resources/rock1.png", f);
		this.comptador++;
		this.id = this.comptador;
		this.name += this.id;
		this.accionsDisponibles = accions;
	}
	
	public Roca(int x1, int y1, int tamany, Field f) {
		
		super("roca", x1, y1, x1+tamany, y1+tamany, 0, "resources/rock1.png", f);
		this.comptador++;
		this.id = this.comptador;
		this.name += this.id;
		this.accionsDisponibles = 50;
	}
	
	public Roca() {
		
		super("roca", 0, 0, 50, 50, 0, "resources/rock1.png", Joc1.f);
		this.comptador++;
		this.id = this.comptador;
		this.name += this.id;
		this.accionsDisponibles = 50;
	}

}

