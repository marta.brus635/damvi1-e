package part4;

import java.awt.Color;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;
import part1.Roca;

public class Projectil extends PhysicBody{

	public static int uwuwu = 0;
	
	public Projectil(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);
		
	}
	
	public Projectil(Projectil p, int x1, int y1, int x2, int y2) {
		super(p.name, x1, y1, x2, y2, p.angle, p.path, p.f);
		this.trigger = true;
	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		if(sprite instanceof Roca || sprite instanceof Projectil){
			this.delete();
			
		}else if(sprite instanceof Enemic) {
			uwuwu++;
			this.delete();
			sprite.delete();
	
		}
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		
	}
}
