package part4;

import Core.Field;
import Core.Sprite;

public class Puntos extends Sprite{
	 static int punts = 0;

	    public Puntos(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f, boolean b) {
	        super(name, x1, y1, x2, y2, angle, path, f);
	        this.text = true;
	        this.textColor = 0xFFFFFF;
	        this.unscrollable = true;
	        this.solid = false;
	    }

	    @Override
	    public void update() {
	        this.path = "Punts: " + punts;
	    }
}
