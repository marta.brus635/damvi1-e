package part4;

import java.util.Random;
import java.util.Timer;

import Core.Field;
import Core.PhysicBody;
import Core.Sprite;

public class Spawner extends PhysicBody{
	
	static Random r;
	
	public Spawner(String name, int x1, int y1, int x2, int y2, double angle, String path, Field f) {
		super(name, x1, y1, x2, y2, angle, path, f);

	}

	@Override
	public void onCollisionEnter(Sprite sprite) {
		
	}

	@Override
	public void onCollisionExit(Sprite sprite) {
		
	}
	
	public static void generarEnemic() {
		
		r = new Random();

		int posicion = r.nextInt(820);
		Enemic enemic = new Enemic("Enemic", 1400, posicion, 1450, posicion+50, 0, "resources/octorok.gif", Joc5.f);
		enemic.setVelocity(-4, 0);
	}
	
}
